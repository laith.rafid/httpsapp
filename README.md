## two way to run this app :
#1.using docker :
 -  docker run -it --rm --name httpsapp -v "$PWD":/usr/src/https/ -w /usr/src/https/ ruby:2.5 ruby httpsapp.rb

#2. or simply using installed ruby 2.5 on your machine :) 
 -  ruby httpsapp.rb
