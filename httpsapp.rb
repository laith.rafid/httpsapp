require 'net/http'
require 'benchmark'

# Question 4
# Can you write a small Ruby based script/app that will get HTTP response times
# over 5 minutes from your location to https://gitlab.com?
#
#i assumed that you meant over 5 minutes = 5 * 60 = 300 seconds over it means
# any number > 300 in this case i chose 400
#
#
# so if you mean over (within 300 seconds ) then i need to change below number

def https_response
      url = 'https://gitlab.com'
      responses = []
      stop_time = Time.now + 300 # this number need to be changed

  begin
    while Time.now < stop_time
      time = Benchmark.realtime do
        response = Net::HTTP.get(URI(url))
      end
      responses.push("HTTP response times over 5 minutes (300 seconds) Response time= #{time*1000.round} in milliseconds"  )
    end
  end
  return responses
end
puts(https_response)